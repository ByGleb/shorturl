﻿using System.Threading.Tasks;

namespace Test.App.URLShortener.Business.Interface
{
    public interface ICounterService<T> : IService<T> where T : class
    {
        Task<T> Update(int id);
    }
}