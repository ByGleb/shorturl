﻿using System.Threading.Tasks;

namespace Test.App.URLShortener.Business.Interface
{
    public interface IService<T>
    {
        Task<T> Get(int id);
        Task<T> Create(T item);
    }
}