﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Test.App.URLShortener.Business.Interface
{
    public interface IUrlInfoService<T> : IService<T> where T : class
    {
        Task<List<T>> GetAll();
        Task Remove(int id);
    }
}