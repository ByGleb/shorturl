﻿using Microsoft.Extensions.DependencyInjection;
using Test.App.URLShortener.Business.Interface;
using Test.App.URLShortener.Business.Services;
using Test.App.URLShortener.Common.Model;

namespace Test.App.URLShortener.Business.DI
{
    public static class BusinessDependencyRegistrator
    {
        public static void RegisterBusinessServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IUrlInfoService<UrlInfo>, UrlInfoService>();

            serviceCollection.AddScoped<ICounterService<Counter>, CounterService>();
        }
    }
}