﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Test.App.URLShortener.Business.Interface;
using Test.App.URLShortener.Common.Model;
using Test.App.URLShortener.Data.Interfaces;

namespace Test.App.URLShortener.Business.Services
{
    public class UrlInfoService : IUrlInfoService<UrlInfo>
    {
        #region static field
        private static readonly int LENGTH_SHORTURL = 6;
        private static readonly string UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static readonly string LOWER = UPPER.ToLower();
        private static readonly string STRING_FOR_GENERATE = string.Concat(UPPER, LOWER);
        #endregion

        private readonly IUrlInfoRepository<UrlInfo> _urlInfoRepository;

        public UrlInfoService(IUrlInfoRepository<UrlInfo> urlInfoRepository)
        {
            _urlInfoRepository = urlInfoRepository;
        }

        public async Task<UrlInfo> Create(UrlInfo urlInfo)
        {
                if (string.IsNullOrEmpty(urlInfo.ShortUrl))
                {
                    urlInfo.ShortUrl = GenerateShortUrl();
                }

                if (!ValidateShortUrl(urlInfo.ShortUrl))
                {
                    return await Task.FromResult<UrlInfo>(null);
                }
                return await _urlInfoRepository.Create(urlInfo);
        }

        public async Task Remove(int id)
        {
            await _urlInfoRepository.Remove(id);
        }

        public Task<List<UrlInfo>> GetAll()
        {
            return _urlInfoRepository.GetAll();
        }

        public Task<UrlInfo> Get(int id)
        {
            return _urlInfoRepository.Get(id);
        }

        private bool ValidateShortUrl(string shortUrl)
        {
            Regex regexForShortUrl = new Regex(@"[a-z,A-z]{6}");

            if (regexForShortUrl.Matches(shortUrl).Count != 1)
            {
                return false;
            }

            var collectionUrlInfo = _urlInfoRepository.GetAll().Result;
            var alignment = collectionUrlInfo.Find(item => item.ShortUrl.Equals(shortUrl));

            if (alignment != null)
            {
                return false;
            }

            return true;
        }

        private string GenerateShortUrl()
        {
            string generatedUrl = string.Empty;
            Random random = new Random();
            do
            {
                for (int i = 0; i < LENGTH_SHORTURL; i++)
                {
                    char symbol = STRING_FOR_GENERATE[random.Next(STRING_FOR_GENERATE.Length)];
                    generatedUrl += symbol;
                }
            }
            while (!ValidateShortUrl(generatedUrl));

            return generatedUrl;
        }

    }
}