﻿using System.Threading.Tasks;
using Test.App.URLShortener.Business.Interface;
using Test.App.URLShortener.Common.Model;
using Test.App.URLShortener.Data.Interfaces;

namespace Test.App.URLShortener.Business.Services
{
    public class CounterService : ICounterService<Counter>
    {
        private readonly ICounterRepository<Counter> _counterRepository;

        public CounterService(ICounterRepository<Counter> counterRepository)
        {
            _counterRepository = counterRepository;
        }

        public async Task<Counter> Update(int id)
        {
            return await _counterRepository.Update(id);
        }

        public Task<Counter> Get(int id)
        {
            return _counterRepository.Get(id);
        }
        public async Task<Counter> Create(Counter item)
        {
            return await _counterRepository.Create(item);
        }
    }
}