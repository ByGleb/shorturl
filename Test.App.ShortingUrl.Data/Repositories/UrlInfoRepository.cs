﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.App.URLShortener.Common.Model;
using Test.App.URLShortener.Data.Interfaces;

namespace Test.App.URLShortener.Data.Repositories
{
    public class UrlInfoRepository : IUrlInfoRepository<UrlInfo>
    {

        private readonly URLShortenerContext _context;

        public UrlInfoRepository(URLShortenerContext context)
        {
            _context = context;
        }

        public async Task<List<UrlInfo>> GetAll()
        {
            var a = await _context.UrlInfos.Include(x=>x.Counter).ToListAsync();
            return a;
        }

        public async Task<UrlInfo> Get(int id)
        {
            return await _context.UrlInfos.Include(x => x.Counter).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<UrlInfo> Create(UrlInfo item)
        {
            var entity = await _context.UrlInfos.FirstOrDefaultAsync(x => x.Id == item.Id);

            if (entity == null)
            {
                item.CreatedData = DateTime.Now;
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        public async Task Remove(int id)
        {
            var entity = await _context.UrlInfos.Include(x => x.Counter).FirstOrDefaultAsync(x => x.Id == id);

            if (entity != null)
            {
                _context.UrlInfos.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }
    }
}
