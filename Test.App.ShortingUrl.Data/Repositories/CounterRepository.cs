﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Test.App.URLShortener.Common.Model;
using Test.App.URLShortener.Data.Interfaces;

namespace Test.App.URLShortener.Data.Repositories
{
    public class CounterRepository : ICounterRepository<Counter>
    {

        private readonly URLShortenerContext _context;

        public CounterRepository(URLShortenerContext context)
        {
            _context = context;
        }

        public async Task<Counter> Get(int id)
        {
            return await _context.Counters.FirstOrDefaultAsync(x => x.UrlInfoId == id);
        }

        public async Task<Counter> Create(Counter item)
        {
            var entity = await _context.Counters.FirstOrDefaultAsync(x => x.UrlInfoId == item.UrlInfoId);

            if (entity == null)
            {
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        public async Task<Counter> Update(int id)
        {
            var entity = await _context.Counters.FirstOrDefaultAsync(x => x.UrlInfoId == id);

            if (entity != null)
            {
                entity.AmountClickLink++;

                await _context.SaveChangesAsync();
                return entity;
            }

            return null;
        }
    }
}
