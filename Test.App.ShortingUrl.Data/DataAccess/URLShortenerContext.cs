﻿using Microsoft.EntityFrameworkCore;
using Test.App.URLShortener.Common.Model;

namespace Test.App.URLShortener.Data
{
    public class URLShortenerContext : DbContext
    {
        public DbSet<Counter> Counters { get; set; }
        public DbSet<UrlInfo> UrlInfos { get; set; }

        public URLShortenerContext(DbContextOptions<URLShortenerContext> options)
            : base(options)
        {
        }
    }
}