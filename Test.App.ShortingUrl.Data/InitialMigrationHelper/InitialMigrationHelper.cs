﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Test.App.URLShortener.Data.InitialMigrationHelper
{
    public static class InitialMigrationHelper
    {
        public static async Task EnsureDatabasesMigrated(IServiceProvider services)
        {
            using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (var context = scope.ServiceProvider.GetRequiredService<URLShortenerContext>())
                {
                    await context.Database.MigrateAsync();
                }
            }
        }
    }
}
