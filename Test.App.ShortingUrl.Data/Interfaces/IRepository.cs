﻿using System.Threading.Tasks;

namespace Test.App.URLShortener.Data.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<T> Get(int id);
        Task<T> Create(T item);
    }
}