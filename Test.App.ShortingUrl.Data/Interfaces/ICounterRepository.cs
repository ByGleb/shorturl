﻿using System.Threading.Tasks;

namespace Test.App.URLShortener.Data.Interfaces
{
    public interface ICounterRepository<T> : IRepository<T> where T : class
    {
        Task<T> Update(int id);
    }
}