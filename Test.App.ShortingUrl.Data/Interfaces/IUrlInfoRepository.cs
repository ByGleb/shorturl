﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Test.App.URLShortener.Data.Interfaces
{
    public interface IUrlInfoRepository<T> : IRepository<T> where T : class
    {
        Task<List<T>> GetAll();
        Task Remove(int id);
    }
}