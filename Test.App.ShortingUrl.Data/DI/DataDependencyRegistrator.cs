﻿using Microsoft.Extensions.DependencyInjection;
using Test.App.URLShortener.Common.Model;
using Test.App.URLShortener.Data.Interfaces;
using Test.App.URLShortener.Data.Repositories;

namespace Test.App.URLShortener.Data.DI
{
    public static class DataDependencyRegistrator
    {
        public static void RegisterDataRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IUrlInfoRepository<UrlInfo>, UrlInfoRepository>();

            serviceCollection.AddScoped<ICounterRepository<Counter>, CounterRepository>();
        }
    }
}