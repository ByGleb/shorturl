﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.App.URLShortener.Business.Interface;
using Test.App.URLShortener.Common.Model;

namespace Test.App.URLShortener.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CounterController : ControllerBase
    {

        private readonly ICounterService<Counter> _counterService;

        public CounterController(ICounterService<Counter> counterService)
        {
            _counterService = counterService;
        }

        /// <summary>
        /// Update counter
        /// </summary>
        /// <remarks>This API endpoint update counter for click URL.</remarks>>
        /// <param name="id">This is Id url</param>
        /// <returns></returns>
        [HttpPut]
        [Route("update/{id}")]
        public async Task<ActionResult> Update(int id)
        {
            var result = await _counterService.Update(id);

            if (result !=null)
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}
