﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.App.URLShortener.Business.Interface;
using Test.App.URLShortener.Common.Model;

namespace Test.App.URLShortener.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrlInfoController : ControllerBase
    {

        private readonly IUrlInfoService<UrlInfo> _urlInfoService;
        private readonly ICounterService<Counter> _counterService;

        public UrlInfoController(IUrlInfoService<UrlInfo> urlInfoService, ICounterService<Counter> counterService)
        {
            _urlInfoService = urlInfoService;
            _counterService = counterService;
        }

        /// <summary>
        /// Create short url
        /// </summary>
        /// <remarks>This API endpoint create record about short url.</remarks>>
        /// <param name="urlInfo">This is a JSON with original url(requared) and shot url(optional).</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(UrlInfo urlInfo)
        {
            var result = await _urlInfoService.Create(urlInfo);
            if (result != null)
            {
                var counter = new Counter()
                {
                    UrlInfoId = result.Id
                };
                await _counterService.Create(counter);
                return Ok(result);
            }
            return BadRequest();
        }

        /// <summary>
        /// Remove short url
        /// </summary>
        /// <remarks>This API endpoint remove record about short url.</remarks>>
        /// <param name="id">This is a Id shot url.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                await _urlInfoService.Remove(id);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get data about url
        /// </summary>
        /// <remarks>This API endpoint return data about url.</remarks>>
        /// <param name="id">This is a Id shot url.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("get-urlinfo-by-id/{id}")]
        public async Task<ActionResult> Get(int id)
        {

            var result = await _urlInfoService.Get(id);

            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest();
        }

        /// <summary>
        /// Get list url
        /// </summary>
        /// <remarks>This API endpoint return list url.</remarks>>
        /// <returns></returns>
        [HttpGet]
        [Route("get-all-urlinfo")]
        public async Task<ActionResult> GetAll()
        {
            var result = await _urlInfoService.GetAll();

            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest();
        }
    }
}