import { Data } from "@angular/router";
import { Counter } from "./Counter";

export class UrlInfo {

  id: number
  longUrl: string
  shortUrl: string
  createDate: Data
  counter: Counter

}
