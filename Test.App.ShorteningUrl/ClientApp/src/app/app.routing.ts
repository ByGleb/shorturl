import { Routes, RouterModule } from '@angular/router';
import { InformationAboutUrlComponent } from './information-about-url/information-about-url.component';
import { CreatShortUrlComponent } from './creat-short-url/creat-short-url.component';


const routes: Routes = [
    { path: '', component: InformationAboutUrlComponent, pathMatch: 'full' },
    { path: 'information', component: InformationAboutUrlComponent },
    { path: 'create-short-url', component: CreatShortUrlComponent }
];

export const AppRouting = RouterModule.forRoot(routes);
