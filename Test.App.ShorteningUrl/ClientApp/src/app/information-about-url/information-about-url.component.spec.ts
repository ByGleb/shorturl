import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationAboutUrlComponent } from './information-about-url.component';

describe('InformationAboutUrlComponent', () => {
  let component: InformationAboutUrlComponent;
  let fixture: ComponentFixture<InformationAboutUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationAboutUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationAboutUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
