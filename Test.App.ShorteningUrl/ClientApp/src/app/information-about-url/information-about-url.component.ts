import { Component, OnInit } from '@angular/core';
import { UrlInfo } from '../../model/UrlInfo';
import { UrlInfoService, CounterService } from '../../services';

@Component({
  selector: 'app-information-about-url',
  templateUrl: './information-about-url.component.html',
  styleUrls: ['./information-about-url.component.css']
})
export class InformationAboutUrlComponent implements OnInit {

  urlInfos: Array<UrlInfo>

  constructor(
    protected urlInfoService: UrlInfoService,
    protected counterService: CounterService
  ) { }

  ngOnInit() {
    this.urlInfos = new Array<UrlInfo>()

    this.urlInfoService.getUrlInfo().subscribe(
      result => {
        this.urlInfos = result
      },
      error => {

      }
    )
  }

  clickOnShortUrl(id) {
    this.counterService.UpdateCounter(id).subscribe(
      result => {
        this.urlInfoService.getUrlInfoById(id).subscribe(
          res => {
            if (res.longUrl.startsWith('https://') || res.longUrl.startsWith("http://")) {
              window.open(res.longUrl)
            } else {
              window.open(`https://${res.longUrl}`);
            }
            this.urlInfos.find(x => x.id == id).counter.amountClickLink = res.counter.amountClickLink
          }
        )
      }
    )
  }

  onRemove(id) {
    this.urlInfoService.deleteUrl(id).subscribe(
      result => {
        let element = this.urlInfos.find(x => x.id == id)
        let indexElement = this.urlInfos.indexOf(element)
        if (this.urlInfos.length == 1) {
          this.urlInfos = new Array<UrlInfo>();
        }
        this.urlInfos.splice(indexElement,indexElement)
      }
    )
  }

}
