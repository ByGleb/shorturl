import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { UrlInfoService, CounterService } from 'src/services';
import { CreatShortUrlComponent } from './creat-short-url/creat-short-url.component';
import { InformationAboutUrlComponent } from './information-about-url/information-about-url.component';
import { AppRouting } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    CreatShortUrlComponent,
    InformationAboutUrlComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    AppRouting
  ],
  providers: [
    CounterService,
    UrlInfoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
