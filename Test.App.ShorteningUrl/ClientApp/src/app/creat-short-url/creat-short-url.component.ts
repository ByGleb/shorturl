import { Component, OnInit } from '@angular/core';
import { UrlInfoService, CounterService } from '../../services';
import { UrlInfo } from '../../model/UrlInfo';

@Component({
  selector: 'app-creat-short-url',
  templateUrl: './creat-short-url.component.html',
  styleUrls: ['./creat-short-url.component.css']
})
export class CreatShortUrlComponent implements OnInit {

  basicUrl: string = ''
  shortUrl: string = ''
  error: string = ''
  success: string = ''

  showError: boolean = false
  showSuccess: boolean = false

  createdUrl: UrlInfo = new UrlInfo()

  constructor(
    private urlInfoService: UrlInfoService,
    private counterService: CounterService
  ) { }

  ngOnInit() {
  }

  onSubmit(form) {

    if (form.invalid) return;

    let model = new UrlInfo();

    model.longUrl = form.value.BasicUrl;
    model.shortUrl = form.value.ShortUrl;

    this.urlInfoService.createShortUrl(model).subscribe(
      result => {
        this.createdUrl = result
        this.showError = false
        this.showSuccess = true
        this.success = `test-app.org/${result.shortUrl}`
      },
      error => {
        this.showSuccess = false
        this.showError = true
        this.error = error
      })
  }

  onCancel() {
    window.location.href = '/'
  }

  clickOnShortUrl() {
    this.counterService.UpdateCounter(this.createdUrl.id).subscribe(
      result => {
        if (this.createdUrl.longUrl.startsWith('https://') || this.createdUrl.longUrl.startsWith("http://")) {
          window.open(this.createdUrl.longUrl)
        } else {
          window.open(`https://${this.createdUrl.longUrl}`);
        }
      }
    )
  }

}


