import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatShortUrlComponent } from './creat-short-url.component';

describe('CreatShortUrlComponent', () => {
  let component: CreatShortUrlComponent;
  let fixture: ComponentFixture<CreatShortUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatShortUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatShortUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
