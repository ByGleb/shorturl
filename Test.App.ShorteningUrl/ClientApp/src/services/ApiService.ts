import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';

import { catchError, switchMap} from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {

  constructor(
    protected http: HttpClient
  ) { }

  handleError(errorResp: HttpErrorResponse): Observable<never> {

    return of(errorResp).pipe(
      switchMap(error => this.handleApplicationError(error)),
      switchMap(error => this.handleServerError(error)),
      switchMap(error => this.handleUnknownError(error)),
      catchError(error => {
        console.error(error);
        return throwError(error);
      })
    )
  }

  handleApplicationError(errorResp: HttpErrorResponse): Observable<any | never> {
    const applicationError = errorResp.headers.get('Application-Error');

    if (applicationError) return throwError(applicationError);

    return of(errorResp);
  }


  handleServerError(errorResp: HttpErrorResponse): Observable<any | never> {
    const serverError = errorResp.error;
    let errorMessage = "";

    if (!serverError || errorResp.status == 400) return throwError("Bad request");

    if (Array.isArray(serverError)) {
      serverError.forEach(item => {
        errorMessage += item + '\n';
      });
    }

    if (serverError.Error) {
      errorMessage += serverError.Error + '\n';
    }

    if (serverError.Reason) {
      errorMessage += serverError.Reason + '\n';
    }

    if (serverError.Message) {
      errorMessage += serverError.Message + '\n';
    }

    errorResp['errorMessage'] = errorMessage;

    return of(errorResp);
  }


  handleUnknownError(errorResp: HttpErrorResponse): Observable<never> {
    return throwError(errorResp);
  }

  get(url: string, options: any = {}): Observable<any> {
    return this.http.get(`${url}`, options).pipe(
      catchError(error => this.handleError(error))
    );
  }

  post(url: string, body: any = {}, options: any = {}): Observable<any> {
    return this.http.post(`${url}`, body, options).pipe(
      catchError(error => this.handleError(error))
    );
  }

  put(url: string, body: any = {}, options: any = {}): Observable<any> {
    return this.http.put(`${url}`, body, options).pipe(
      catchError(error => this.handleError(error))
    );
  }

  delete(url: string, options: any = {}): Observable<any> {
    return this.http.delete(`${url}`, options).pipe(
      catchError(error => this.handleError(error))
    );
  }
}
