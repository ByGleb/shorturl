import { Observable } from 'rxjs';
import { ApiService } from './ApiService';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CounterService extends ApiService {
  url = 'api/counter'

  constructor(
    protected http: HttpClient
  ) {
    super(http)
  }

  UpdateCounter(id: number): Observable<any> {
    return this.put(`${this.url}/update/${id}`)
  }
}
