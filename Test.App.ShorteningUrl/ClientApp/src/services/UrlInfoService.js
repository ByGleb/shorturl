"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/common/http");
var ApiService_1 = require("./ApiService");
var UrlInfoService = /** @class */ (function (_super) {
    __extends(UrlInfoService, _super);
    function UrlInfoService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.url = 'api/urlinfo';
        return _this;
    }
    UrlInfoService.prototype.getUrlInfoById = function (id) {
        var params = new http_1.HttpParams().set('id', id.toString()), options = { params: params };
        return this.get(this.url, options);
    };
    UrlInfoService.prototype.getUrlInfo = function () {
        return this.get(this.url);
    };
    UrlInfoService.prototype.post = function (body, options) {
        if (body === void 0) { body = {}; }
        if (options === void 0) { options = {}; }
        return this.post(this.url, body);
    };
    UrlInfoService.prototype.deletePublicApi = function (id) {
        return this.delete(this.url);
    };
    return UrlInfoService;
}(ApiService_1.ApiService));
exports.UrlInfoService = UrlInfoService;
//# sourceMappingURL=UrlInfoService.js.map