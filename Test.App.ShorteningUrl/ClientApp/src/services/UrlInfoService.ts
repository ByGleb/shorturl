import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './ApiService';
import { Injectable } from '@angular/core';

@Injectable()
export class UrlInfoService extends ApiService {
  url = 'api/urlinfo'

  constructor(
    protected http: HttpClient
  ) {
    super(http)
  }

  getUrlInfoById(id: number): Observable<any> {
    return this.get(`${this.url}/get-urlinfo-by-id/${id}`)
  }

  getUrlInfo(): Observable<any> {
    return this.get(`${this.url}/get-all-urlinfo`)
  }

  createShortUrl(body: any = {}): Observable<any> {
    return this.post(this.url, body)
  }

  deleteUrl(id: number): Observable<any> {
    return this.delete(`${this.url}/delete/${id}`)
  }
}
