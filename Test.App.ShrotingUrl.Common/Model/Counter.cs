﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.App.URLShortener.Common.Model
{
    public class Counter
    {
        public int AmountClickLink { get; set; }
        [Key]
        public int UrlInfoId { get; set; }

        public string Test { get; set; }
    }
}
