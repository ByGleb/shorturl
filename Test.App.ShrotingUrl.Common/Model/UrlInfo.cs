﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Test.App.URLShortener.Common.Model
{
    public class UrlInfo
    {
        public int Id { get; set; }

        [Required]
        public string LongUrl { get; set; }
        public string ShortUrl { get; set; }
        public DateTime CreatedData { get; set; }
        public Counter Counter { get; set; }
    }
}
